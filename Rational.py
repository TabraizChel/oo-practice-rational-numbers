#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 12:20:58 2018

@author: chelt
"""
from math import gcd




                  
             
             

class Rational(object):
    
    def __init__(self,numerator, denominator = None):
        if denominator == None:
            j = 0
            x=''
            y=''
            while True: 
                if numerator[j] == '/':
                    break
                x += numerator[j]
                j+=1
            while True:
                j+=1
                try:
                    y += numerator[j]
                except IndexError:
                    break
            g = gcd(int(x), int(y))
            self.numerator = int(x) // g
            self.denominator = int(y) // g                
        else:  
            g = gcd(numerator, denominator)
            self.numerator = numerator // g
            self.denominator = denominator // g
            
    
            
    def __add__(self, b):
        if type(b) == int:
            b = Rational(b,1)
            
        
        x  = self.numerator*b.denominator + b.numerator*self.denominator
        y  = b.denominator*self.denominator
        
        return Rational(x,y)
    
    def __sub__(self, b):
        if type(b) == int:
            b = Rational(b,1)
            
        if self.numerator == b.numerator and self.denominator == b.denominator:
            return 0
        
        x  = self.numerator*b.denominator - b.numerator*self.denominator
        y  = b.denominator*self.denominator
        return Rational(x,y)
    
    def __mul__(self,b):
        
        if type(b) == int:
            b = Rational(b,1)
        
        x  = self.numerator*b.numerator
        y  = b.denominator*self.denominator
        return Rational(x,y)
    
    def __truediv__(self,b):
        
        if type(b) == int:
            b = Rational(b,1)
        
        if self.numerator == b.numerator and self.denominator == b.denominator:
            return 1
        x  = self.numerator*b.denominator
        y  = b.numerator*self.denominator  
        return Rational(x,y)
        
        
    
    def __str__(self):
        return "{}/{}".format(self.numerator,self.denominator)
        
    
z = Rational('300/900')
y = Rational(1,2)
print(y / 2)